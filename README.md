**This project is for the DevOps Bootcamp for "Operating Systems & Linux Basics" Module**

<details>
<summary>Exercise 1: Linux Virtual Machine </summary>

<br />

Linux Distro:  

<code>$cat /etc/os-release <br>
PRETTY_NAME="Ubuntu 22.04.4 LTS"
NAME="Ubuntu"
VERSION_ID="22.04"
VERSION="22.04.4 LTS (Jammy Jellyfish)"
VERSION_CODENAME=jammy
ID=ubuntu
ID_LIKE=debian
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
UBUNTU_CODENAME=jammy
</code>

<code>Vim editor</code>


<code>$ echo $SHELL
/bin/bash
</code>
</details>

***************

<details>
<summary>Exercise 2: Bash Script - Install Java </summary>
 <br />

**script:**
```sh
#!/bin/bash

apt update
apt install default-jre -y

java_version=$(java -version 2>&1 > /dev/null | grep "java version\|openjdk version" | awk '{print substr($3,2,2)}')

if [ "$java_version" == "" ]
then
  echo "Java was not installed"
elif [ "$java_version" -lt 11 ]
then
  echo "An old version was installed"
elif [ "$java_version" -ge 11 ]
then
  echo "Java version 11 or higher was installed"	
fi
```
</details>

***********
<details>
<summary>Exercise 3: Bash Script - List user processes</summary>
 <br />

**script:**

```sh
#!/bin/bash

echo "List of Proscesses for user: $USER"

ps aux | grep -i `whoami`

```
</details>

***********
<details>
<summary>Exercise 4: Bash Script - List user processes sorted by Memory or CPU</summary>
 <br />

**script:**

```sh
#!/bin/bash

read -p "Would like to sort the processes by Memory or CPU.(m/c)? " sortby

if [ "$sortby" == "m" ]
then
   ps aux --sort -rss | grep -i `whoami`
elif [ "$sortby" == "c" ]
then
   ps aux --sort -%cpu | grep -i `whoami`
else
  echo "No input provided. Exiting" 
fi

```
</details>

***********

<details>
<summary>Exercise 5: Bash Script - List user processes sorted by Memory or CPU and number of line to list</summary>
 <br />

**script:**

```sh
#!/bin/bash

read -p "Would like to sort the processes by Memory or CPU.(m/c)? " sortby

read -p "How many results do you want to display? " lines

if [ "$sortby" == "m" ]
then
   ps aux --sort -rss | grep -i `whoami` | head -n "$lines"
elif [ "$sortby" == "c" ]
then
   ps aux --sort -%cpu | grep -i `whoami` | head -n "$lines"
else
  echo "No input provided. Exiting" 
fi

```
</details>

***********


<details>
<summary>Exercise 6 and 7: Install and configure node app</summary>
 <br />

**script:**

```sh
#!/bin/bash

echo "Install node, nmp, curl, wget , net-tools"
apt update
apt install -y nodejs npm curl net-tools wget
echo ""
echo "################"
echo ""

echo "Configure log directory for the application (absolute path): "
read LOG_DIR

if [ -d "$LOG_DIR" ]
then
   echo "The directory already exists"
else
   mkdir -p $LOG_DIR
   echo "A new directory was created"
fi

# display Node JS version
node_version=$(node --version)
echo "NodeJS version $node_version installed"

#display nmp version

npm_version=$(npm --version)
echo "NPM version $npm_version installed"


#Download NodeJS project 
wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

#extrac the software
tar zxvf ./bootcamp-node-envvars-project-1.0.0.tgz

# Configure all env variables
export APPENV=dev
export DB_USER=myuser
export DB_PWD=mysecret

#move to package dir to install
cd package

#Run installation

npm install

#Start nodejs

node server.js &

#Display the processes

ps aux| grep node | grep -v grep

#Display port listneting

netstat -lntp | grep :3000

```
</details>

***********

<details>
<summary>Exercise 8 and 9</summary>
 <br />

**script:**
```sh
#!/bin/bash

# prepare environment, install all tools
apt update
NEW_USER=myapp

echo "install node, npm, curl, wget, net-tools"
apt install -y nodejs npm curl net-tools  
sleep 15
echo ""
echo "################"
echo ""
# read user input for log directory
echo -n "Set log directory location for the application (absolute path): "

read LOG_DIRECTORY
if [ -d $LOG_DIRECTORY ]
then
else
  mkdir -p $LOG_DIRECTORY
  echo "A new directory $LOG_DIRECTORY has been created"
fi

# display nodeJS version
node_version=$(node --version)
echo "NodeJS version $node_version installed"

# display npm version
npm_version=$(npm --version)
  echo "$LOG_DIRECTORY already exists"
echo "NPM version $npm_version installed"

echo ""
echo "################"
echo ""

# create new user to run the application and make owner of log dir
useradd $NEW_USER -m
chown $NEW_USER -R $LOG_DIRECTORY

# executing the following commands as new user using 'runuser' command

# fetch NodeJS project archive from s3 bucket
runuser -l $NEW_USER -c "wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz"

# extract the project archive to ./package folder
runuser -l $NEW_USER -c "tar zxvf ./bootcamp-node-envvars-project-1.0.0.tgz"

# start the nodejs application in the background, with all needed env vars with new user myapp
runuser -l $NEW_USER -c "
    export APP_ENV=dev && 
    export DB_PWD=mysecret && 
    export DB_USER=myuser && 
    export LOG_DIR=$LOG_DIRECTORY && 
    cd package && 
    npm install && 
    node server.js &"

# display that nodejs process is running
ps aux | grep node | grep -v grep

# display that nodejs is running on port 3000
netstat -ltnp | grep :3000
```
</details>

***********




